(function (window, document) {

    var layout     = document.getElementById('layout'),
        menu       = document.getElementById('menu'),
        menuLink   = document.getElementById('menuLink'),
        menuToggle = document.getElementById('menuToggle'),
        content    = document.getElementById('main');

    function toggleClass(element, className) {
        var classes = element.className.split(/\s+/),
            length = classes.length,
            i = 0;

        for(; i < length; i++) {
          if (classes[i] === className) {
            classes.splice(i, 1);
            break;
          }
        }
        // The className is not found
        if (length === classes.length) {
            classes.push(className);
        }

        element.className = classes.join(' ');
    }

    function toggleAll(e) {
        var active = 'active';

        e.preventDefault();
        toggleClass(layout, active);
        toggleClass(menu, active);
        toggleClass(menuLink, active);
    }

    function toggleMenu(e) {
        var inactive = 'inactive';
        e.preventDefault();
        toggleClass(menu, inactive);
    }

    menuLink.onclick = function menuLinkClick(e) {
        toggleAll(e);
    };

    menuToggle.onclick = function menuToggleClick(e) {
        toggleMenu(e);
    }

    content.onclick = function contentClick(e) {
        var classes = menu.className.split(/\s+/);

        if (classes.includes('active')) {
            toggleAll(e);
        }
    };

}(window, window.document));
