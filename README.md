# Lighten the Web

[![Code Shelter](https://www.codeshelter.co/static/badges/badge-flat.svg)](https://www.codeshelter.co/)

Lighten the Web is an informational resource with tips on how to speed up your websites.

To compile the website, first install Lektor and then just run `lektor server`. That's it!